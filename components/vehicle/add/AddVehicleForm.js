import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Stepper,
  Step,
  StepLabel,
  Button,
  Typography,
  Grid,
  FormGroup,
} from "@material-ui/core";
import FormStp1 from "./FormStp1";
import styles from "./AddVehicleForm.module.scss";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return [
    "Select master blaster campaign settings",
    "Create an ad group",
    "Create an ad",
  ];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <FormStp1 />;
    case 1:
      return "What is an ad group anyways?";
    case 2:
      return "This is the bit I really care about!";
    default:
      return "Unknown stepIndex";
  }
}

export default function AddVehicleForm() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const formSubmitHandler = (e) => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    e.preventDefault();
    console.log(e);
  };

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  console.log(activeStep, steps.length);

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <form onSubmit={formSubmitHandler}>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              Votre annonce a été créée
            </Typography>
            <Button onClick={handleReset}>Créer une nouvelle annonce</Button>
          </div>
        ) : (
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            spacing={3}
          >
            {getStepContent(activeStep)}
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                Précédent
              </Button>
              <Button variant="contained" color="primary" onClick={handleNext}>
                {activeStep === steps.length - 1 ? "Envoyer" : "Suivant"}
              </Button>
            </div>
          </Grid>
        )}
        {activeStep === steps.length - 1 ? 
        <Button type="submit">Envoyer</Button> : ""
        
        }

      </form>
    </div>
  );
}
