import { Card, Input, InputBase, InputLabel } from "@material-ui/core";
import styles from "./AddVehicleForm.module.scss";

const FormStp1 = () => {
  return (
    <div className={styles.inputs}>
        <InputLabel>Plaque d'immatriculation</InputLabel>
        <Input type="text" required />
    </div>
  );
};

export default FormStp1;
