import Image from "next/image";
import styles from "../styles/Home.module.scss";
import { Button, Grid } from "@material-ui/core";
import PageTemplate from "../components/Layout/PageTemplate/PageTemplate";
import { Container } from "@material-ui/core";

export default function Home() {
  return (
    <>
      <PageTemplate
        className={styles.home}
        metaDescription="Réseau privée de vente de véhicules entre professionnels"
      >
        <Container>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={3}
          >
            <Grid item xs={12} sm={6}>
              <h1>Réseau privé de vente de véhicules entre professionnels</h1>
              <Button variant="contained" color="primary" size="large">
                Demandez votre adhésion
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={3}
              >
                <Image
                  src="/assets/img/Car_Monochromatic.png"
                  alt="Picture of the author"
                  width={500}
                  height={500}
                />
              </Grid>
            </Grid>
          </Grid>
        </Container>

        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={3}
          className={styles.CTA1}
        >
          <Button variant="contained" color="primary" size="large">
            Demandez votre adhésion
          </Button>{" "}
        </Grid>

        <Container>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={3}
          >
            <Grid item xs={12} sm={6}>
              <h1>Réseau privé de vente de véhicules entre professionnels</h1>
              <Button variant="contained" color="primary" size="large">
                Demandez votre adhésion
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={3}
              >
                <Image
                  src="/assets/img/Car_Monochromatic.png"
                  alt="Picture of the author"
                  width={500}
                  height={500}
                />
              </Grid>
            </Grid>
          </Grid>
        </Container>


      </PageTemplate>
    </>
  );
}
