import React from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { Button, TextField, Paper, Grid } from "@material-ui/core";
import PageTemplate from "../../components/Layout/PageTemplate/PageTemplate";
import classes from "../../styles/forms.module.scss";

const validationSchema = yup.object({
  email: yup
    .string("Entrez votre email")
    .email("Merci d'entrer un email valide")
    .required("Merci d'entrer votre email"),
  password: yup
    .string("Entrez un mot de passe")
    .min(8, "Le mot de passe doit faire 8 caractères minimum")
    .required("Merci d'entrer votre mot de passe"),
  passwordConfirmation: yup
    .string()
    .required("Merci d'entrer la confirmation de mot de passe")
    .oneOf([yup.ref("password"), null]
    , "Le mot de passe doivent être identiques"),
});

export default function Register() {
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      passwordConfirmation: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <PageTemplate>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={3}
      >
        <div className={classes.formCard}>
          <h1>Connexion</h1>
          <form onSubmit={formik.handleSubmit}>
            <div className={classes.formControl}>
              <TextField
                fullWidth
                id="email"
                name="email"
                label="Email"
                value={formik.values.email}
                onChange={formik.handleChange}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
              />
            </div>
            <div className={classes.formControl}>
              <TextField
                fullWidth
                id="password"
                name="password"
                label="Mot de passe"
                type="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
              />
            </div>
            <div className={classes.formControl}>
              <TextField
                fullWidth
                id="passwordConfirmation"
                name="passwordConfirmation"
                label="Confirmez votre mot de passe"
                type="password"
                value={formik.values.confirmPassword}
                onChange={formik.handleChange}
                error={
                  formik.touched.passwordConfirmation && Boolean(formik.errors.passwordConfirmation)
                }
                helperText={formik.touched.passwordConfirmation && formik.errors.passwordConfirmation}
              />
            </div>
            <div className={classes.formAction}>
              <Button
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
              >
                Se connecter
              </Button>
            </div>
          </form>
        </div>
      </Grid>
    </PageTemplate>
  );
}
