import { useRouter } from "next/router";
import PageTemplate from "../../../components/Layout/PageTemplate/PageTemplate";

export default function VehicleAd() {
  const router = useRouter();

  const vehicleID = router.query.vehicleId

  // send request to backend to fetch the data of the vehicle with vehicleId

  return (
    <PageTemplate>
      <h1>VehicleAd</h1>
      <p>{vehicleID}</p>
    </PageTemplate>
  );
}
