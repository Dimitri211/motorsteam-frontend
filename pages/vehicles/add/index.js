import { Container, Grid, Card } from "@material-ui/core";
import { useRouter } from "next/router";
import PageTemplate from "../../../components/Layout/PageTemplate/PageTemplate";
import AddVehicleForm from "../../../components/vehicle/add/AddVehicleForm";
import styles from "./addVehicle.module.scss";

export default function AddVehicle() {
  return (
    <PageTemplate>
      <Container className={styles.main}>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          spacing={3}
        >
          <Card className={styles.card}>
            <h1>Ajouter un véhicule</h1>
            <AddVehicleForm />
          </Card>
        </Grid>
      </Container>
    </PageTemplate>
  );
}
