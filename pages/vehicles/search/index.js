import Link from "next/link";
import PageTemplate from "../../../components/Layout/PageTemplate/PageTemplate";

export default function SearchVehicle() {
  const vehicles = [
    {
      id: 1,
      model: "peugeot",
    },
    {
      id: 2,
      model: "mercedes",
    },
  ];

  return (
    <PageTemplate>
      <ul>
        {vehicles.map((vehicle) => (
          <li key={vehicle.id}>
            <Link href={`/vehicles/${vehicle.id}`}>{vehicle.model}</Link>
          </li>
        ))}
      </ul>
    </PageTemplate>
  );
}
